package me.kodysimpson.serialization;

import me.kodysimpson.serialization.commands.SerializeCommand;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Base64;

public final class Serialization extends JavaPlugin {

    @Override
    public void onEnable() {
        // Plugin startup logic

        getCommand("serialize").setExecutor(new SerializeCommand());

    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
