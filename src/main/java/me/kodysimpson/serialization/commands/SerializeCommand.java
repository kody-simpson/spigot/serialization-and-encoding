package me.kodysimpson.serialization.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;

import java.io.*;
import java.util.Base64;

public class SerializeCommand implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (sender instanceof Player){

            Player p = (Player) sender;

            ItemStack item = p.getInventory().getItemInMainHand();

            String encodedObject;

            try{
                //Serialize the item(turn it into byte stream)
                ByteArrayOutputStream io = new ByteArrayOutputStream();
                BukkitObjectOutputStream os = new BukkitObjectOutputStream(io);
                os.writeObject(item);
                os.flush();

                byte[] serializedObject = io.toByteArray();

                //Encode the serialized object into to the base64 format
                encodedObject = new String(Base64.getEncoder().encode(serializedObject));

                p.sendMessage("Encoded Item: " + encodedObject);

                //Remove the item from their main hand
                p.getInventory().setItemInMainHand(null);

                //Now we are going to do the reverse: decode the string back into raw bytes
                //and then deserialize the byte array into an object

                //decode string into raw bytes
                serializedObject = Base64.getDecoder().decode(encodedObject);

                //Input stream to read the byte array
                ByteArrayInputStream in = new ByteArrayInputStream(serializedObject);
                //object input stream to serialize bytes into objects
                BukkitObjectInputStream is = new BukkitObjectInputStream(in);

                //Use the object input stream to deserialize an object from the raw bytes
                ItemStack newItem = (ItemStack) is.readObject();

                p.getInventory().setItemInMainHand(newItem);

            }catch (IOException | ClassNotFoundException ex){
                System.out.println(ex);
            }

        }


        return true;
    }
}
